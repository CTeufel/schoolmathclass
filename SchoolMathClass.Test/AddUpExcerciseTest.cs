﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolMathClass.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SchoolMathClass.Test
{
    [TestClass]
    public class AddUpExcerciseTest
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void RandomDifficultyTest()
        {
            var output = new StringBuilder();

            output.Append(TestContext.TestName);
            output.AppendLine(EDifficultyLevel.Easy.ToString());

            var statisticDifficultyEasy = new Dictionary<int, int>();

            for (int i = 0; i < 200; i++)
            {
                var excercise = new AddUpExcercise();

                output.AppendLine(excercise.ToString());

                if (statisticDifficultyEasy.ContainsKey(excercise.CorrectResult))
                {
                    statisticDifficultyEasy[excercise.CorrectResult] = statisticDifficultyEasy[excercise.CorrectResult] + 1;
                }
                else
                {
                    statisticDifficultyEasy[excercise.CorrectResult] = 1;
                }
            }

            output.AppendLine("---------Statistics---------------");

            foreach (KeyValuePair<int, int> kvp in statisticDifficultyEasy)
            {
                output.AppendLine(string.Format("Number {0}\t({1})", kvp.Key, kvp.Value));
            }

            output.AppendLine("------------------------");
            output.AppendLine("------------------------");

            output.AppendLine(EDifficultyLevel.Hard.ToString());

            var statisticDifficultyHard = new Dictionary<int, int>();

            for (int i = 0; i < 100; i++)
            {
                var excercise = new AddUpExcercise(EDifficultyLevel.Hard);

                output.AppendLine(excercise.ToString());

                if (statisticDifficultyHard.ContainsKey(excercise.CorrectResult))
                {
                    statisticDifficultyHard[excercise.CorrectResult] = statisticDifficultyHard[excercise.CorrectResult] + 1;
                }
                else
                {
                    statisticDifficultyHard[excercise.CorrectResult] = 1;
                }
            }

            output.AppendLine("---------Statistics---------------");

            foreach (KeyValuePair<int, int> kvp in statisticDifficultyHard)
            {
                output.AppendLine(string.Format("Number {0}\t({1})", kvp.Key, kvp.Value));
            }

            Console.Out.WriteLine(output);


            // Convinience : open in txt file
            var fileName = string.Format("{0}{1}", TestContext.TestName, ".txt");

            if (File.Exists(fileName))
                File.Delete(fileName);

            using (var file = File.CreateText(fileName))
            {
                file.Write(output.ToString());
            }

            System.Diagnostics.Process.Start(fileName);
        }

        [TestMethod]
        public void DifficultyLevelTest()
        {
            var excercise = new AddUpExcercise();

            Assert.IsTrue(excercise.DifficultyLevel == EDifficultyLevel.Easy);

            excercise = new AddUpExcercise(EDifficultyLevel.Easy);

            Assert.IsTrue(excercise.DifficultyLevel == EDifficultyLevel.Easy);

            excercise = new AddUpExcercise(EDifficultyLevel.Hard);

            Assert.IsTrue(excercise.DifficultyLevel == EDifficultyLevel.Hard);
        }

        [TestMethod]
        public void ResultTest()
        {
            var excercise = new AddUpExcercise();

            Assert.AreEqual(excercise.OperandA + excercise.OperandB, excercise.CorrectResult);
        }

        [TestMethod]
        public void WrongResultsTest()
        {
            var excercise = new AddUpExcercise();

            Assert.AreNotEqual(excercise.WrongResult1, excercise.WrongResult2);
        }
    }
}
