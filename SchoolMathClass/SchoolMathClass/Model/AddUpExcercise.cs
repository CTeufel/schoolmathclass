﻿using SchoolMathClass.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SchoolMathClass.Model
{
    public class AddUpExcercise: Excercise
    {
        public AddUpExcercise(EDifficultyLevel difficultyLevel = EDifficultyLevel.Easy) : base (difficultyLevel)
        {
        }

        public override string Operator { get { return "+"; } }

        public override void Calculate(int difficultyLevelAsInt)
        {
            CorrectResult = difficultyLevelAsInt + 1;

            while (CorrectResult > difficultyLevelAsInt) // Ensure result is not greater than difficultyLevel
            {
                OperandA = RandomNumbers.Next(0, difficultyLevelAsInt);
                OperandB = RandomNumbers.Next(0, difficultyLevelAsInt);
                CorrectResult = OperandA + OperandB;
            }           

            WrongResult2 = CorrectResult + 1;

            WrongResult1 = (CorrectResult - 1 < 1) ? WrongResult2 + 1 : CorrectResult - 1; // Do not allow 0 value
        }
    }
}
