﻿using SchoolMathClass.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SchoolMathClass.Model
{
    public class SubstractExcercise : Excercise
    {
        public SubstractExcercise(EDifficultyLevel difficultyLevel = EDifficultyLevel.Easy) : base(difficultyLevel)
        {
        }

        public override string Operator { get { return "-"; } }

        public override void Calculate(int difficultyLevelAsInt)
        {
            CorrectResult = difficultyLevelAsInt + 1;
                        
            OperandA = RandomNumbers.Next(0, difficultyLevelAsInt);
            OperandB = RandomNumbers.Next(0, difficultyLevelAsInt);

            if (OperandB > OperandA) // Ensure OperandA is always greater than OperandB
            {
                var temp = OperandA;
                OperandA = OperandB;
                OperandB = temp;
            }

            CorrectResult = OperandA - OperandB;
            

            WrongResult2 = CorrectResult + 1;

            WrongResult1 = (CorrectResult - 1 < 0) ? WrongResult2 + 1 : CorrectResult - 1; // Do not allow results less than 0
        }
    }
}
