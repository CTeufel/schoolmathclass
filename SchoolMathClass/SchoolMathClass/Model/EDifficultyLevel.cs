﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolMathClass.Model
{
    public enum EDifficultyLevel
    {
        Easy = 15,
        Hard = 35
    }
}
