﻿using SchoolMathClass.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolMathClass.Model
{
    public abstract class Excercise
    {
        public Excercise(EDifficultyLevel difficultyLevel = EDifficultyLevel.Easy)
        {
            DifficultyLevel = difficultyLevel;

            Calculate((int)DifficultyLevel);
        }

        public EDifficultyLevel DifficultyLevel { get; private set; }

        public abstract string Operator { get; }

        public int OperandA { get; set; }

        public int OperandB { get; set; }

        public int CorrectResult { get; set; }

        public int WrongResult1 { get; set; }

        public int WrongResult2 { get; set; }

        public abstract void Calculate(int difficultyLevelAsInt);

        public override string ToString()
        {
            return string.Format("{0} {5} {1} = {2} and not {3} or {4}", OperandA, OperandB, CorrectResult, WrongResult1, WrongResult2, Operator);
        }
    }
}
