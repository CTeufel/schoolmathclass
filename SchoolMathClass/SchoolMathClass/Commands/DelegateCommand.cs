﻿using System;
using System.Windows.Input;

public class DelegateCommand : ICommand
{
    private readonly Action _execute;

    private readonly Func<bool> _canExecute;

    public DelegateCommand(Action execute, Func<bool> canExecute)
    {
        _execute = execute;

        if (canExecute != null)
        {
            _canExecute = canExecute;
        }
    }

    public DelegateCommand(Action execute)
        : this(execute, null)
    {
    }

    public event EventHandler CanExecuteChanged;

    public void RaiseCanExecuteChanged()
    {
        CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

    public bool CanExecute(object parameter)
    {
        return _canExecute == null || _canExecute.Invoke();
    }

    public virtual void Execute(object parameter)
    {
        if (CanExecute(parameter))
        {
            _execute.Invoke();
        }
    }
}

public class DelegateCommand<T> : ICommand
{
    private readonly Action<T> _execute;

    private readonly Predicate<T> _canExecute;

    public DelegateCommand(Action<T> execute)
        : this(execute, null)
    {
    }

    public DelegateCommand(Action<T> execute, Predicate<T> canExecute)
    {
        _execute = execute;

        if (canExecute != null)
        {
            _canExecute = canExecute;
        }
    }

    public event EventHandler CanExecuteChanged;

    public void RaiseCanExecuteChanged()
    {
        CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

    public bool CanExecute(object parameter)
    {
        return _canExecute == null || _canExecute.Invoke((T)parameter);
    }

    public virtual void Execute(object parameter)
    {
        if (CanExecute(parameter))
        {
            _execute((T)parameter);
        }
    }
}