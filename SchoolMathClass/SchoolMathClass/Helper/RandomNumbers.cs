﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolMathClass.Helper
{
    internal static class RandomNumbers
    {
        static int _lastRandomNumber;

        static int _calls;

        static int _maxCalls = 15;

        static Random _random = CreateRandom();

        internal static int Next(int minValue, int maxValue)
        {
            var randomNumber = _random.Next(minValue, maxValue);

            // every once in a while return same number and reset random
            if (_calls > _maxCalls)
            {
                _calls = 0;

                CreateRandom();

                return _lastRandomNumber;                
            }
            else
            {
                _calls = _calls + 1;

                _lastRandomNumber = randomNumber;

                return randomNumber;
            }
        }

        static Random CreateRandom()
        {
            _random = new Random(DateTime.Now.Ticks.GetHashCode());

            return _random;
        }
    }
}
