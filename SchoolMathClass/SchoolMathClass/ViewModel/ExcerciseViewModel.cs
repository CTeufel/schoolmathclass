﻿using SchoolMathClass.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace SchoolMathClass.ViewModel
{
    public class ExcerciseViewModel: ViewModelBase
    {
        public ExcerciseViewModel()
        {
            InitializeResults();
        }              

        Excercise _excercise;

        public Excercise Excercise
        {
            get { return _excercise; }
            set
            {
                _excercise = value;

                NotifyPropertyChanged();
            }
        }

        int _value1;

        public int Value1
        {
            get { return _value1; }
            set
            {
                _value1 = value;

                NotifyPropertyChanged();
            }
        }

        int _value2;

        public int Value2
        {
            get { return _value2; }
            set
            {
                _value2 = value;

                NotifyPropertyChanged();
            }
        }

        int _value3;

        public int Value3
        {
            get { return _value3; }
            set
            {
                _value3 = value;

                NotifyPropertyChanged();
            }
        }

        bool _canShowNextExcercise;

        public bool CanShowNextExcercise
        {
            get { return _canShowNextExcercise; }
            set
            {
                _canShowNextExcercise = value;

                NotifyPropertyChanged();
            }
        }

        bool _isHardDifficulty;

        public bool IsHardDifficulty
        {
            get { return _isHardDifficulty; }
            set
            {
                _isHardDifficulty = value;

                NotifyPropertyChanged();
            }
        }

        #region Commands
        private ICommand _valueSelectedCommand;

        public ICommand ValueSelectedCommand
        {
            get
            {
                if (null == _valueSelectedCommand)
                    _valueSelectedCommand = new DelegateCommand<int>(OnValueSelectedCommand);

                return _valueSelectedCommand;
            }
        }

        private void OnValueSelectedCommand(int selectedValue)
        {
            CanShowNextExcercise = selectedValue == Excercise.CorrectResult;
        }

        private ICommand _showNextExcerciseCommand;

        public ICommand ShowNextExcerciseCommand
        {
            get
            {
                if (null == _showNextExcerciseCommand)
                    _showNextExcerciseCommand = new DelegateCommand(OnShowNextExcerciseCommand);

                return _showNextExcerciseCommand;
            }
        }

        private void OnShowNextExcerciseCommand()
        {
            InitializeResults();
        }

        #endregion

        private void InitializeResults()
        {
            CanShowNextExcercise = false;

            if (Helper.RandomNumbers.Next(1, 3) == 1)
                Excercise = new AddUpExcercise(IsHardDifficulty ? EDifficultyLevel.Hard : EDifficultyLevel.Easy);
            else
                Excercise = new SubstractExcercise(IsHardDifficulty ? EDifficultyLevel.Hard : EDifficultyLevel.Easy);

            int position = Helper.RandomNumbers.Next(1, 4);

            if (position == 1)
            {
                Value1 = Excercise.CorrectResult;
                Value2 = Excercise.WrongResult1;
                Value3 = Excercise.WrongResult2;
            }
            else if (position == 2)
            {
                Value2 = Excercise.CorrectResult;
                Value1 = Excercise.WrongResult1;
                Value3 = Excercise.WrongResult2;
            }
            else
            {
                Value3 = Excercise.CorrectResult;
                Value2 = Excercise.WrongResult1;
                Value1 = Excercise.WrongResult2;
            }
        }
    }
}
