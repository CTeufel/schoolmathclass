﻿using SchoolMathClass.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace SchoolMathClass
{
    public class MainPageViewModel: ViewModelBase
    {
        public MainPageViewModel()
        {
            SumUpExcercise = new ExcerciseViewModel();
        }

        #region Commands
        private ICommand _closeApplicationCommand;

        public ICommand CloseApplicationCommand
        {
            get
            {
                if (null == _closeApplicationCommand)
                    _closeApplicationCommand = new DelegateCommand(OnExecuteCloseApplicationCommand);

                return _closeApplicationCommand;
            }
        }

        private void OnExecuteCloseApplicationCommand()
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        #endregion

        #region Properties

        ExcerciseViewModel _sumUpExcercise;

        public ExcerciseViewModel SumUpExcercise 
        { 
            get
            {
                return _sumUpExcercise;
            }
            set
            {
                _sumUpExcercise = value;

                NotifyPropertyChanged();
            }
        }

        #endregion
    }
}
